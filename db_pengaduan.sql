-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Apr 10, 2020 at 10:26 PM
-- Server version: 5.7.29-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_pengaduan`
--

-- --------------------------------------------------------

--
-- Table structure for table `detail_kelas`
--

CREATE TABLE `detail_kelas` (
  `nis` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_tingkat` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_kelas` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_jurusan` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `jurusan`
--

CREATE TABLE `jurusan` (
  `id_jurusan` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_singkat` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_lengkap` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `jurusan`
--

INSERT INTO `jurusan` (`id_jurusan`, `nama_singkat`, `nama_lengkap`, `created_at`, `updated_at`) VALUES
('KJ-001', 'RPL', 'Rekayasa Perangkat Lunak', '2020-03-02 06:51:10', '2020-03-02 06:51:10'),
('KJ-002', 'TP', 'Teknik Pendingin', '2020-03-02 06:51:27', '2020-03-02 06:51:27');

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id_kelas` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`id_kelas`, `nama`, `created_at`, `updated_at`) VALUES
('KK-001', 'A', '2020-03-02 06:50:27', '2020-03-02 06:50:47'),
('KK-002', 'B', '2020-03-02 06:50:33', '2020-03-02 06:50:51'),
('KK-003', 'C', '2020-03-02 06:50:55', '2020-03-02 06:50:55'),
('KK-004', 'D', '2020-03-02 06:50:59', '2020-03-02 06:50:59');

-- --------------------------------------------------------

--
-- Table structure for table `komentar`
--

CREATE TABLE `komentar` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `id_pengaduan` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nis` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `komentar` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2020_02_10_090853_create_petugas_table', 1),
(2, '2020_02_10_091909_create_siswas_table', 1),
(3, '2020_02_10_092354_create_kelas_table', 1),
(4, '2020_02_10_092546_create_tingkats_table', 1),
(5, '2020_02_10_092701_create_jurusans_table', 1),
(6, '2020_02_10_092800_create_detail_kelas_table', 1),
(7, '2020_02_10_093114_create_pengaduans_table', 1),
(8, '2020_02_10_093600_create_tanggapans_table', 1),
(9, '2020_02_10_093851_create_pesans_table', 1),
(10, '2020_02_18_071008_create_komentars_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `pengaduan`
--

CREATE TABLE `pengaduan` (
  `id_pengaduan` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nis` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `judul` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `isi_laporan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `foto` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` enum('pending','proses','selesai') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pesan`
--

CREATE TABLE `pesan` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_pengaduan` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `idp` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `waktu` datetime NOT NULL DEFAULT '2020-02-10 09:59:30',
  `pesan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `petugas`
--

CREATE TABLE `petugas` (
  `id_petugas` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `jk` enum('L','P') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'L',
  `telp` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_lahir` date NOT NULL,
  `level` enum('Administrator','Petugas') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'Petugas',
  `username` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Stand-in structure for view `show_pesan`
-- (See below for the actual view)
--
CREATE TABLE `show_pesan` (
`siswa` varchar(30)
,`petugas` varchar(30)
,`id` int(10) unsigned
,`id_pengaduan` varchar(12)
,`idp` varchar(12)
,`waktu` datetime
,`pesan` text
,`created_at` timestamp
,`updated_at` timestamp
);

-- --------------------------------------------------------

--
-- Table structure for table `siswa`
--

CREATE TABLE `siswa` (
  `nis` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jk` enum('L','P') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'L',
  `telp` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_lahir` date NOT NULL,
  `username` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Stand-in structure for view `siswa_kelas`
-- (See below for the actual view)
--
CREATE TABLE `siswa_kelas` (
`nis` varchar(20)
,`nama` varchar(30)
,`jk` enum('L','P')
,`telp` varchar(15)
,`tgl_lahir` date
,`username` varchar(20)
,`password` varchar(50)
,`created_at` timestamp
,`updated_at` timestamp
,`id_tingkat` varchar(12)
,`id_kelas` varchar(12)
,`id_jurusan` varchar(12)
,`tingkat` varchar(10)
,`jurusan` varchar(10)
,`kelas` varchar(10)
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `siswa_komentar`
-- (See below for the actual view)
--
CREATE TABLE `siswa_komentar` (
`nama` varchar(30)
,`id` bigint(20) unsigned
,`id_pengaduan` varchar(12)
,`nis` varchar(20)
,`komentar` varchar(255)
,`created_at` timestamp
,`updated_at` timestamp
);

-- --------------------------------------------------------

--
-- Stand-in structure for view `siswa_pengaduan`
-- (See below for the actual view)
--
CREATE TABLE `siswa_pengaduan` (
`nama` varchar(30)
,`tingkat` varchar(10)
,`kelas` varchar(10)
,`jurusan` varchar(10)
,`id_pengaduan` varchar(12)
,`nis` varchar(20)
,`tanggal` date
,`judul` varchar(30)
,`isi_laporan` text
,`foto` varchar(255)
,`status` enum('pending','proses','selesai')
,`created_at` timestamp
,`updated_at` timestamp
);

-- --------------------------------------------------------

--
-- Table structure for table `tanggapan`
--

CREATE TABLE `tanggapan` (
  `id_tanggapan` int(11) NOT NULL,
  `id_pengaduan` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tanggal` date NOT NULL,
  `tanggapan` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_petugas` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Stand-in structure for view `tanggapan_petugas`
-- (See below for the actual view)
--
CREATE TABLE `tanggapan_petugas` (
`nama` varchar(30)
,`id_tanggapan` int(11)
,`id_pengaduan` varchar(12)
,`tanggal` date
,`tanggapan` text
,`id_petugas` varchar(12)
,`created_at` timestamp
,`updated_at` timestamp
);

-- --------------------------------------------------------

--
-- Table structure for table `tingkat`
--

CREATE TABLE `tingkat` (
  `id_tingkat` varchar(12) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `tingkat`
--

INSERT INTO `tingkat` (`id_tingkat`, `nama`, `created_at`, `updated_at`) VALUES
('KT-001', 'X', '2020-03-02 06:48:55', '2020-03-02 06:48:55'),
('KT-002', 'XII', '2020-03-02 06:49:59', '2020-03-02 06:49:59');

-- --------------------------------------------------------

--
-- Structure for view `show_pesan`
--
DROP TABLE IF EXISTS `show_pesan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `show_pesan`  AS  select `c`.`nama` AS `siswa`,`b`.`nama` AS `petugas`,`a`.`id` AS `id`,`a`.`id_pengaduan` AS `id_pengaduan`,`a`.`idp` AS `idp`,`a`.`waktu` AS `waktu`,`a`.`pesan` AS `pesan`,`a`.`created_at` AS `created_at`,`a`.`updated_at` AS `updated_at` from ((`pesan` `a` left join `petugas` `b` on((`a`.`idp` = `b`.`id_petugas`))) left join `siswa` `c` on((`a`.`idp` = `c`.`nis`))) ;

-- --------------------------------------------------------

--
-- Structure for view `siswa_kelas`
--
DROP TABLE IF EXISTS `siswa_kelas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `siswa_kelas`  AS  select `a`.`nis` AS `nis`,`a`.`nama` AS `nama`,`a`.`jk` AS `jk`,`a`.`telp` AS `telp`,`a`.`tgl_lahir` AS `tgl_lahir`,`a`.`username` AS `username`,`a`.`password` AS `password`,`a`.`created_at` AS `created_at`,`a`.`updated_at` AS `updated_at`,`b`.`id_tingkat` AS `id_tingkat`,`b`.`id_kelas` AS `id_kelas`,`b`.`id_jurusan` AS `id_jurusan`,`c`.`nama` AS `tingkat`,`d`.`nama_singkat` AS `jurusan`,`e`.`nama` AS `kelas` from ((((`siswa` `a` left join `detail_kelas` `b` on((`a`.`nis` = `b`.`nis`))) left join `tingkat` `c` on((`b`.`id_tingkat` = `c`.`id_tingkat`))) left join `jurusan` `d` on((`b`.`id_jurusan` = `d`.`id_jurusan`))) left join `kelas` `e` on((`b`.`id_kelas` = `e`.`id_kelas`))) ;

-- --------------------------------------------------------

--
-- Structure for view `siswa_komentar`
--
DROP TABLE IF EXISTS `siswa_komentar`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `siswa_komentar`  AS  select `b`.`nama` AS `nama`,`a`.`id` AS `id`,`a`.`id_pengaduan` AS `id_pengaduan`,`a`.`nis` AS `nis`,`a`.`komentar` AS `komentar`,`a`.`created_at` AS `created_at`,`a`.`updated_at` AS `updated_at` from (`komentar` `a` left join `siswa` `b` on((`a`.`nis` = `b`.`nis`))) ;

-- --------------------------------------------------------

--
-- Structure for view `siswa_pengaduan`
--
DROP TABLE IF EXISTS `siswa_pengaduan`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `siswa_pengaduan`  AS  select `b`.`nama` AS `nama`,`d`.`nama` AS `tingkat`,`e`.`nama` AS `kelas`,`f`.`nama_singkat` AS `jurusan`,`a`.`id_pengaduan` AS `id_pengaduan`,`a`.`nis` AS `nis`,`a`.`tanggal` AS `tanggal`,`a`.`judul` AS `judul`,`a`.`isi_laporan` AS `isi_laporan`,`a`.`foto` AS `foto`,`a`.`status` AS `status`,`a`.`created_at` AS `created_at`,`a`.`updated_at` AS `updated_at` from (((((`pengaduan` `a` left join `siswa` `b` on((`a`.`nis` = `b`.`nis`))) left join `detail_kelas` `c` on((`b`.`nis` = `c`.`nis`))) left join `tingkat` `d` on((`c`.`id_tingkat` = `d`.`id_tingkat`))) left join `kelas` `e` on((`c`.`id_kelas` = `e`.`id_kelas`))) left join `jurusan` `f` on((`c`.`id_jurusan` = `f`.`id_jurusan`))) ;

-- --------------------------------------------------------

--
-- Structure for view `tanggapan_petugas`
--
DROP TABLE IF EXISTS `tanggapan_petugas`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `tanggapan_petugas`  AS  select `c`.`nama` AS `nama`,`a`.`id_tanggapan` AS `id_tanggapan`,`a`.`id_pengaduan` AS `id_pengaduan`,`a`.`tanggal` AS `tanggal`,`a`.`tanggapan` AS `tanggapan`,`a`.`id_petugas` AS `id_petugas`,`a`.`created_at` AS `created_at`,`a`.`updated_at` AS `updated_at` from (`tanggapan` `a` left join `petugas` `c` on((`a`.`id_petugas` = `c`.`id_petugas`))) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `detail_kelas`
--
ALTER TABLE `detail_kelas`
  ADD PRIMARY KEY (`nis`);

--
-- Indexes for table `jurusan`
--
ALTER TABLE `jurusan`
  ADD PRIMARY KEY (`id_jurusan`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id_kelas`);

--
-- Indexes for table `komentar`
--
ALTER TABLE `komentar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengaduan`
--
ALTER TABLE `pengaduan`
  ADD PRIMARY KEY (`id_pengaduan`);

--
-- Indexes for table `pesan`
--
ALTER TABLE `pesan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `petugas`
--
ALTER TABLE `petugas`
  ADD PRIMARY KEY (`id_petugas`);

--
-- Indexes for table `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`nis`);

--
-- Indexes for table `tanggapan`
--
ALTER TABLE `tanggapan`
  ADD PRIMARY KEY (`id_tanggapan`);

--
-- Indexes for table `tingkat`
--
ALTER TABLE `tingkat`
  ADD PRIMARY KEY (`id_tingkat`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `komentar`
--
ALTER TABLE `komentar`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `pesan`
--
ALTER TABLE `pesan`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;
--
-- AUTO_INCREMENT for table `tanggapan`
--
ALTER TABLE `tanggapan`
  MODIFY `id_tanggapan` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
