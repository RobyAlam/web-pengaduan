<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePetugasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('petugas', function (Blueprint $table) {
            $table->string('id_petugas', 12);
            $table->string('nama', 30);
            $table->text('alamat');
            $table->enum('jk', ['L', 'P'])->default('L');
            $table->string('telp', 15);
            $table->date('tgl_lahir');
            $table->enum('level', ['Administrator', 'Petugas'])->default('Petugas');
            $table->string('username', 20)->nullable();
            $table->string('password', 50)->nullable();
            $table->primary('id_petugas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('petugas');
    }
}
