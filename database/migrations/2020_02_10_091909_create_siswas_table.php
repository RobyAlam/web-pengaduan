<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswa', function (Blueprint $table) {
            $table->string('nis', 20);
            $table->string('nama', 30);
            $table->enum('jk', ['L', 'P'])->default('L');
            $table->string('telp', 15);
            $table->date('tgl_lahir');
            $table->string('username', 20)->nullable();
            $table->string('password', 50)->nullable();
            $table->primary('nis');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siswa');
    }
}
