<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tingkat extends Model
{
    public $incrementing = false;
    protected $table = "tingkat";
    protected $primaryKey = "id_tingkat";
    protected $fillable = ['id_tingkat', 'nama'];

}
