<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tanggapan extends Model
{
    public $incrementing = false;
    protected $table = "tanggapan";
    protected $primaryKey = "id_tanggapan";
    protected $fillable = ['id_tanggapan', 'id_pengaduan',
    'tanggal', 'tanggapan', 'id_petugas'];

}
