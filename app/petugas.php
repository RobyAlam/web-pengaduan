<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class petugas extends Model
{
    //
    public $incrementing = false;
    protected $table = "petugas";
    protected $primaryKey = "id_petugas";
    protected $fillable = ['id_petugas', 'nama', 'alamat',
    'telp', 'jk', 'tgl_lahir', 'level', 'username', 'password'];
}
