<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class siswa extends Model
{
    public $incrementing = false;
    protected $table = "siswa";
    protected $primaryKey = "nis";
    protected $fillable = ['nis', 'nama', 'jk',
    'telp', 'tgl_lahir', 'level', 'username', 'password'];

}
