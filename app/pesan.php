<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pesan extends Model
{
    protected $table = "pesan";
    protected $primaryKey = "id";
    protected $fillable = ['id', 'id_pengaduan', 'idp',
    'waktu', 'pesan'];

}
