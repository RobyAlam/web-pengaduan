<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class pengaduan extends Model
{
    public $incrementing = false;
    protected $table = "pengaduan";
    protected $primaryKey = "id_pengaduan";
    protected $fillable = ['id_pengaduan', 'nis',
    'tanggal', 'judul', 'isi_laporan',
    'foto', 'status'];

}
