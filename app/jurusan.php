<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class jurusan extends Model
{
    public $incrementing = false;
    protected $table = "jurusan";
    protected $primaryKey = "id_jurusan";
    protected $fillable = ['id_jurusan', 'nama_singkat', 'nama_lengkap'];

}
