<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detail_kelas extends Model
{
    public $incrementing = false;
    protected $table = "detail_kelas";
    protected $primaryKey = "nis";
    protected $fillable = ['nis', 'id_tingkat',
    'id_kelas', 'id_jurusan'];

}
