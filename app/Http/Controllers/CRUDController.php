<?php

namespace App\Http\Controllers;

use App\detail_kelas;
use App\jurusan;
use App\siswa;
use App\kelas;
use App\komentar;
use App\pengaduan;
use App\pesan;
use App\petugas;
use App\tanggapan;
use App\tingkat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File as File;
use PDF;
use RealRashid\SweetAlert\Facades\Alert;

class CRUDController extends Controller
{
    public function otomatis($tabel, $primary, $prefix)
    {
        $sumber = DB::table($tabel)->get();
        $output = "";
        foreach ($sumber as $s) {
            $output = substr($s->$primary, 4, 6);
            $output = $output+1;
        }
        if($output < 1) {
            $output = $prefix."001";
        } elseif($output < 10) {
            $output = $prefix."00".$output;
        } elseif($output < 100) {
            $output = $prefix."0".$output;
        } else {
            $output = $prefix.$output;
        }
        return $output;
    }

    public function KodeOtomatis()
    {
        $id_petugas = $this->otomatis('petugas', 'id_petugas', 'IP-');
        $id_tingkat = $this->otomatis('tingkat', 'id_tingkat', 'KT-');
        $id_jurusan = $this->otomatis('jurusan', 'id_jurusan', 'KJ-');
        $id_kelas = $this->otomatis('kelas', 'id_kelas', 'KK-');
        $sumber = DB::table('pengaduan')->get();
        $id_pengaduan = "";
        $tglsekarang = "";
        foreach ($sumber as $s) {
            $tglsekarang =  substr($s->id_pengaduan, 3, 6);
            $id_pengaduan = substr($s->id_pengaduan, 10, 12);
            $id_pengaduan = $id_pengaduan + 1;
            if ($tglsekarang != now()->format('Ym')) {
                $id_pengaduan = 1;
            }
        }
        if($id_pengaduan < 1) {
            $id_pengaduan = "PG-".now()->format('Ym')."001";
        } elseif($id_pengaduan < 10) {
            $id_pengaduan = "PG-".now()->format('Ym')."00".$id_pengaduan;
        } elseif($id_pengaduan < 100) {
            $id_pengaduan = "PG-".now()->format('Ym')."0".$id_pengaduan;
        } else {
            $id_pengaduan = "PG-".now()->format('Ym').$id_pengaduan;
        }
        $data = array(
            'id_petugas' => $id_petugas,
            'id_tingkat' => $id_tingkat,
            'id_jurusan' => $id_jurusan,
            'id_kelas' => $id_kelas,
            'id_pengaduan' => $id_pengaduan
        );
        return $data;
    }
    public function login(Request $request)
    {
        $username = $request->username;
        $password = sha1($request->password);
        $petugas = petugas::where('username', '=', $username)->where('password', '=', $password)->get();
        $id = ""; $hak = "";
        if (count($petugas) > 0) {
            foreach($petugas as $p){$id= $p->id_petugas; $level=$p->level;}
            $request->session()->put('id', $id);
            $request->session()->put('level', $level);
            if ($level == "Petugas") {
                return redirect('/Petugas/Dashboard');
            } else {
                return redirect('/Administrator/Dashboard');
            }
        } else {
            $siswa = siswa::where('username', '=', $username)->where('password', '=', $password)->get();
            if (count($siswa) > 0) {
                foreach($siswa as $p){$id= $p->nis; $level="Siswa";}
                $request->session()->put('id', $id);
                $request->session()->put('level', $level);
                return redirect('/Siswa/Dashboard');
            } else {
                return redirect()->back();
            }
        }
    }

    public function siswa_kelas()
    {
        $siswa = DB::table('siswa_kelas')->get();
        return $siswa;
    }

    public function pengaduan()
    {
        $pengaduan = DB::table('siswa_pengaduan')->orderBy('created_at', 'ASC')->get();
        return $pengaduan;
    }

    public function CariPengaduan($key)
    {
        $pengaduan = DB::select("SELECT * FROM siswa_pengaduan WHERE judul LIKE '%$key%' ORDER BY created_at ASC");
        return $pengaduan;
    }

    public function pengaduanLimit()
    {
        $pengaduan = DB::table('siswa_pengaduan')->orderBy('created_at', 'ASC')->take(10)->get();
        return $pengaduan;
    }

    public function komentar($id_pengaduan)
    {
        $komentar = DB::table('siswa_komentar')->where('id_pengaduan', '=', $id_pengaduan)->orderBy('created_at', 'ASC')->get();
        return $komentar;
    }

    public function tanggapan($id_pengaduan)
    {
        $tanggapan = DB::table('tanggapan_petugas')->where('id_pengaduan', '=', $id_pengaduan)->orderBy('created_at', 'ASC')->get();
        return $tanggapan;
    }

    public function tanggapansemua()
    {
        $tanggapan = DB::table('tanggapan_petugas')->orderBy('created_at', 'ASC')->limit(5)->get();
        return $tanggapan;
    }

    public function pesan($id_pengaduan)
    {
        $pesan = DB::table('show_pesan')->where('id_pengaduan', '=', $id_pengaduan)->orderBy('created_at', 'ASC')->get();
        return $pesan;
    }

    public function pengaduanID($id_pengaduan)
    {
        $pengaduan = DB::table('siswa_pengaduan')->where('id_pengaduan', '=', $id_pengaduan)->orderBy('created_at', 'ASC')->get();
        return $pengaduan;
    }

    public function pengaduansaya(Request $request)
    {
        $nis = $request->session()->get('id');
        $pengaduan = DB::table('siswa_pengaduan')->where('nis', '=',$nis)->get();
        return $pengaduan;
    }

    public function tingkat()
    {
        $tingkat = tingkat::all();
        return $tingkat;
    }

    public function kelas()
    {
        $kelas = kelas::all();
        return $kelas;
    }

    public function jurusan()
    {
        $jurusan = jurusan::all();
        return $jurusan;
    }

    public function petugas() {
        $petugas = petugas::all();
        return $petugas;
    }

    public function userinfo(Request $request)
    {
        $id = $request->session()->get('id');
        $level = $request->session()->get('level');
        if ($id == null) {
            return redirect('/logout');
        }
        $datalogin = array();
        if ($level != "Siswa") {
            $hak = petugas::find($id);
        } else {
            $hak = siswa::find($id);
        }
        $datalogin = array('id' => $id, 'level' => $level, 'nama' => $hak->nama);
        return $datalogin;
    }



    public function CariNIS($nis)
    {
        $siswa = DB::select('SELECT * FROM siswa WHERE nis=? AND username is null AND password is null', [$nis]);
        return $siswa;
    }
    // ================= FUNGSI DELETE =========================
    public function DeleteSiswa(Request $request)
    {
        DB::transaction(function () use($request) {
            $nis = $request->nis;
            $siswa = siswa::find($nis);
            $detail_kelas = detail_kelas::find($nis);
            $detail_kelas->delete();
            $siswa->delete();
        });
    }

    public function DeleteTingkat(Request $request)
    {
        DB::transaction(function () use($request) {
            $id_tingkat = $request->id_tingkat;
            $tingkat = tingkat::find($id_tingkat);
            $tingkat->delete();
        });
    }

    public function DeleteKelas(Request $request)
    {
        DB::transaction(function () use($request) {
            $id_kelas = $request->id_kelas;
            $kelas = kelas::find($id_kelas);
            $kelas->delete();
        });
    }

    public function DeleteJurusan(Request $request)
    {
        DB::transaction(function () use($request) {
            $id_jurusan = $request->id_jurusan;
            $jurusan = jurusan::find($id_jurusan);
            $jurusan->delete();
        });
    }

    public function DeletePetugas(Request $request)
    {
        DB::transaction(function () use($request) {
            $id_petugas = $request->id_petugas;
            $petugas = petugas::find($id_petugas);
            $petugas->delete();
            File::delete('dist/img/petugas/' . $request->id_petugas);
        });
    }
    // ================= END OF FUNGSI DELETE =========================

    // ================= FUNGSI INSERT DAN UPDATE =========================
    public function InsertSiswa(Request $request)
    {
        DB::transaction(function () use($request) {
            $siswa = siswa::where('nis', '=', $request->nis)->get();
            $detail_kelas = detail_kelas::where('nis', '=', $request->nis)->get();
            $data = array(
                'nis' => $request->nis,
                'nama' => $request->nama,
                'jk' => $request->jk,
                'telp' => $request->telp,
                'tgl_lahir' => $request->tgl_lahir,
            );
            $datakelas = array(
                'nis' => $request->nis,
                'id_tingkat' => $request->id_tingkat,
                'id_jurusan' => $request->id_jurusan,
                'id_kelas' => $request->id_kelas,
            );
            if (count($siswa) > 0) {
                $siswa = siswa::where('nis', '=', $request->nis)->update($data);
            } else {
                $siswa = siswa::create($data);
            }
            if (count($detail_kelas) > 0) {
                $detail_kelas = detail_kelas::where('nis', '=', $request->nis)->update($datakelas);
            } else {
                $detail_kelas = detail_kelas::create($datakelas);
            }
        });
    }

    public function InsertTingkat(Request $request)
    {
        DB::transaction(function () use($request) {
            $data = array(
                'id_tingkat' => $request->id_tingkat,
                'nama' => $request->nama
            );
            $tingkat = tingkat::where('id_tingkat', '=', $request->id_tingkat)->get();
            if (count($tingkat) > 0) {
                $tingkat = tingkat::where('nama', '=', $request->nama)->get();
                if (count($tingkat) == 0) {
                    $tingkat = tingkat::where('id_tingkat', '=', $request->id_tingkat)->update($data);
                } else {
                    //UNTUK MEMBUAT ERROR KARENA NAMA TINGKAT SUDAH DIGUNAKAN
                    tingkat::create();
                }
            } else {
                $tingkat = tingkat::where('nama', '=', $request->nama)->get();
                if (count($tingkat) == 0) {
                    $tingkat = tingkat::create($data);
                } else {
                    //UNTUK MEMBUAT ERROR KARENA NAMA TINGKAT SUDAH DIGUNAKAN
                    tingkat::create();
                }
            }
        });
    }


    public function InsertKelas(Request $request)
    {
        DB::transaction(function () use($request) {
            $data = array(
                'id_kelas' => $request->id_kelas,
                'nama' => $request->nama
            );
            $kelas = kelas::where('id_kelas', '=', $request->id_kelas)->get();
            if (count($kelas) > 0) {
                $kelas = kelas::where('nama', '=', $request->nama)->get();
                if (count($kelas) == 0) {
                    $kelas = kelas::where('id_kelas', '=', $request->id_kelas)->update($data);
                } else {
                    //UNTUK MEMBUAT ERROR KARENA NAMA TINGKAT SUDAH DIGUNAKAN
                    kelas::create();
                }
            } else {
                $kelas = kelas::where('nama', '=', $request->nama)->get();
                if (count($kelas) == 0) {
                    $kelas = kelas::create($data);
                } else {
                    //UNTUK MEMBUAT ERROR KARENA NAMA TINGKAT SUDAH DIGUNAKAN
                    kelas::create();
                }
            }
        });
    }


    public function InsertJurusan(Request $request)
    {
        DB::transaction(function () use($request) {
            $data = array(
                'id_jurusan' => $request->id_jurusan,
                'nama_singkat' => $request->nama_singkat,
                'nama_lengkap' => $request->nama_lengkap
            );
            $jurusan = jurusan::where('id_jurusan', '=', $request->id_jurusan)->get();
            if (count($jurusan) > 0) {
                $jurusan = jurusan::where('nama_singkat', '=', $request->nama_singkat)->get();
                if (count($jurusan) == 0) {
                    $jurusan = jurusan::where('id_jurusan', '=', $request->id_jurusan)->update($data);
                } else {
                    //UNTUK MEMBUAT ERROR KARENA NAMA TINGKAT SUDAH DIGUNAKAN
                    jurusan::create();
                }
            } else {
                $jurusan = jurusan::where('nama_singkat', '=', $request->nama_singkat)->get();
                if (count($jurusan) == 0) {
                    $jurusan = jurusan::create($data);
                } else {
                    //UNTUK MEMBUAT ERROR KARENA NAMA TINGKAT SUDAH DIGUNAKAN
                    jurusan::create();
                }
            }
        });
    }


    public function InsertPetugas(Request $request)
    {
        DB::transaction(function () use($request) {
            $file = $request->file('file');
            $uploadlocation = 'dist/img/petugas';
            $petugas = petugas::where('id_petugas', '=', $request->id_petugas)->get();
            if (count($petugas) > 0) {
                if ($file != null) {
                    File::delete('dist/img/petugas/' . $request->id_petugas);
                    $file->move($uploadlocation, $request->id_petugas);
                }
                $data = array(
                    'id_petugas' => $request->id_petugas,
                    'nama' => $request->nama,
                    'level' => $request->level,
                    'jk' => $request->jk,
                    'telp' => $request->telp,
                    'tgl_lahir' => $request->tgl_lahir,
                    'alamat' => $request->alamat,
                );
                $petugas = petugas::where('id_petugas', '=', $request->id_petugas)->update($data);
            } else {
                $data = array(
                    'id_petugas' => $request->id_petugas,
                    'nama' => $request->nama,
                    'level' => $request->level,
                    'jk' => $request->jk,
                    'telp' => $request->telp,
                    'tgl_lahir' => $request->tgl_lahir,
                    'alamat' => $request->alamat,
                    'username' => $request->id_petugas,
                    'password' => sha1($request->id_petugas)
                );
                $petugas = petugas::create($data);
                $file->move($uploadlocation, $request->id_petugas);
            }
        });
    }

    public function InsertKomentar(Request $request)
    {
        DB::transaction(function () use($request) {
            $data = array(
                'id_pengaduan' => $request->id_pengaduan,
                'nis' => $request->nis,
                'komentar' => $request->komentar
            );
            $komentar = komentar::create($data);
        });
    }

    public function InsertTanggapan(Request $request)
    {
        DB::transaction(function () use($request) {
            $data = array(
                'id_pengaduan' => $request->id_pengaduan,
                'id_petugas' => $request->id_petugas,
                'tanggapan' => $request->tanggapan,
                'tanggal' => now()
            );
            $tanggapan = tanggapan::create($data);
            $id_pengaduan = $request->id_pengaduan;
            $pengaduan = pengaduan::where('id_pengaduan', '=', $id_pengaduan)->update([
                'status' => 'Proses',
            ]);
        });
    }

    public function InsertPesan(Request $request)
    {
        DB::transaction(function () use($request) {
            $data = array(
                'id_pengaduan' => $request->id_pengaduan,
                'idp' => $request->idp,
                'waktu' => now(),
                'pesan' => $request->pesan
            );
            $pesan = pesan::create($data);
        });
    }


    public function InsertPengaduan(Request $request)
    {
        DB::transaction(function () use($request) {
            $file = $request->file('file');
            $uploadlocation = 'dist/img/attachments';
            $data = array(
                'id_pengaduan' => $request->id_pengaduan,
                'nis' => $request->nis,
                'tanggal' => now(),
                'judul' => $request->judul,
                'isi_laporan' => $request->isi_laporan,
                'foto' => $request->id_pengaduan,
                'status' => 'pending'
            );
            $pengaduan = pengaduan::create($data);
            $file->move($uploadlocation, $request->id_pengaduan);
        });
    }

    public function UpdateStatus(Request $request)
    {
        DB::transaction(function () use($request) {
            $data = array(
                'id_pengaduan' => $request->id_pengaduan,
                'status' => 'selesai'
            );
            $pengaduan = pengaduan::where('id_pengaduan', '=', $request->id_pengaduan)->update($data);
        });
    }

    public function BuatAkun(Request $request)
    {
        DB::transaction(function () use($request) {
            $file = $request->file('file');
            $uploadlocation = 'dist/img/siswa';
            $username = $request->username;
            $data = array(
                'nis' => $request->nis,
                'username' => $request->username,
                'password' => sha1($request->password)
            );
            $cek = siswa::where('username', '=', $username)->get();
            if (count($cek) > 0) {
                $siswa = siswa::where('nis', '=', $request->nis)->update(['dasda' => 'ddsd']);
            } else {
                $cek = petugas::where('username', '=', $username)->get();
                if (count($cek) > 0) {
                    $siswa = siswa::where('nis', '=', $request->nis)->update(['dasda' => 'ddsd']);
                } else {
                    $siswa = siswa::where('nis', '=', $request->nis)->update($data);
                    $file->move($uploadlocation, $request->nis);
                }
            }
        });
    }

    public function UpdateAkunSiswa(Request $request)
    {
        DB::transaction(function () use($request) {
            $username = $request->username;
            $data = array(
                'nis' => $request->id,
                'username' => $request->username,
                'password' => sha1($request->password)
            );
            $cek = siswa::where('username', '=', $username)->get();
            if (count($cek) > 0) {
                $siswa = siswa::where('nis', '=', $request->nis)->update(['dasda' => 'ddsd']);
            } else {
                $cek = petugas::where('username', '=', $username)->get();
                if (count($cek) > 0) {
                    $siswa = siswa::where('nis', '=', $request->nis)->update(['dasda' => 'ddsd']);
                } else {
                    $siswa = siswa::where('nis', '=', $request->id)->update($data);
                }
            }
        });
    }

    public function UpdateAkunPetugas(Request $request)
    {
        DB::transaction(function () use($request) {
            $username = $request->username;
            $data = array(
                'id_petugas' => $request->id,
                'username' => $request->username,
                'password' => sha1($request->password)
            );
            $cek = petugas::where('username', '=', $username)->get();
            if (count($cek) > 0) {
                DB::rollback();
            } else {
                $cek = siswa::where('username', '=', $username)->get();
                if (count($cek) > 0) {
                    DB::rollback();
                } else {
                    $petugas = petugas::where('id_petugas', '=', $request->id)->update($data);
                }
            }
        });
    }
    // ============== END OF INSERT ========================


    // ============== FUNGSI LAPORAN ========================

    public function LaporanPetugas()
    {
        $petugas = petugas::all();
        $pdf = PDF::loadview('/Laporan/petugas', ['petugas' => $petugas]);
        return $pdf->stream();
    }

    public function LaporanSemuaSiswa()
    {
        $siswa_kelas = DB::table('siswa_kelas')->get();
        $pdf = PDF::loadview('/Laporan/siswa', ['siswa' => $siswa_kelas]);
        return $pdf->stream();
    }

    public function LaporanSemuaPengaduan()
    {
        $pengaduan = DB::table('siswa_pengaduan')->get();
        $pdf = PDF::loadview('/Laporan/pengaduan', ['pengaduan' => $pengaduan]);
        return $pdf->stream();
    }

    public function LaporanSiswa(Request $request)
    {
        $query = "SELECT * FROM siswa_kelas WHERE id_tingkat = '$request->id_tingkat' AND id_jurusan = '$request->id_jurusan' AND id_kelas = '$request->id_kelas'";
        $siswa_kelas = DB::select($query);
        $pdf = PDF::loadview('/Laporan/siswa', ['siswa' => $siswa_kelas]);
        return $pdf->stream();
    }

    public function LaporanPengaduan(Request $request)
    {
        $status = $request->status;
        $tgl_awal = $request->tgl_awal;
        $tgl_akhir = $request->tgl_akhir;
        $st = "";
        if ($status != "semua") {
            $st = "status = '$status'";
        }

        $tgl = "";
        if ($tgl_awal != "" && $tgl_akhir != "") {
            $tgl = "tanggal BETWEEN '$tgl_awal' AND '$tgl_akhir'";
        }
        if ($st == "" && $tgl == "") {
            $query = "SELECT * FROM siswa_pengaduan";
        } elseif ($st != "" && $tgl == "") {
            $query = "SELECT * FROM siswa_pengaduan WHERE $st";
        } elseif ($st == "" && $tgl != "") {
            $query = "SELECT * FROM siswa_pengaduan WHERE $tgl";
        } else {
            $query = "SELECT * FROM siswa_pengaduan WHERE $st AND $tgl";
        }
        $pengaduan = DB::select($query);
        $pdf = PDF::loadview('/Laporan/pengaduan', ['pengaduan' => $pengaduan]);
        return $pdf->stream();
    }
    // ============== END OF FUNGSI LAPORAN =================


}
