<?php

namespace App\Http\Controllers;


use App\petugas;
use App\siswa;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use Session;


class ViewController extends Controller
{
    //
    public function userinfo(Request $request) {
        $id = $request->session()->get('id');
        $level = $request->session()->get('level');
        if ($id == null) {
            return redirect('/logout');
        }
        $datalogin = array('id' => $id, 'level' => $level);
        return $datalogin;
    }

    public function Petugas(Request $request)
    {
        $this->userinfo($request);
        return view('dashboard');
    }
    public function Admin(Request $request)
    {
        $this->userinfo($request);
        return view('dashboard');
    }
    public function Siswa(Request $request)
    {
        $this->userinfo($request);
        return view('dashboard');
    }
}
