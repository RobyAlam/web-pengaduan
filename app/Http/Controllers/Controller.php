<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;
use Session;
use Alert;
use App\siswa;
use App\petugas;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function cekakun()
    {
        $petugas = petugas::where('level', '=', 'Administrator')->get();
        if (count($petugas) > 0) {
            return view('/login');
        } else {
            return redirect('/BuatAkun');
        }
    }

    public function formbuatakun()
    {
        return view('/firstpage');
    }

    public function BuatAdmin(Request $request)
    {
        DB::transaction(function () use($request) {
            $data = array(
                'id_petugas' => 'IP-001',
                'nama' => $request->nama,
                'alamat' => $request->alamat,
                'jk' => $request->jk,
                'telp' => $request->telp,
                'tgl_lahir' => $request->tgl_lahir,
                'level' => 'Administrator',
                'username' => $request->username,
                'password' => sha1($request->password)
            );

            $insert = petugas::create($data);
        });
        return redirect('/');
    }

    public function login(Request $request) {
        $username = $request->username;
        $password = sha1($request->password);
        $petugas = petugas::where('username', '=', $username)->where('password', '=', $password)->get();
        $id = ""; $hak = "";
        if (count($petugas) > 0) {
            foreach($petugas as $p){$id= $p->id_petugas; $level=$p->level;}
            $request->session()->put('id', $id);
            $request->session()->put('level', $level);
            return redirect('/Dashboard');
        } else {
            $siswa = siswa::where('username', '=', $username)->where('password', '=', $password)->get();
            if (count($siswa) > 0) {
                foreach($siswa as $p){$id= $p->nis; $level="Siswa";}
                $request->session()->put('id', $id);
                $request->session()->put('level', $level);
                return redirect('/Dashboard');
            } else {
                Alert::error('Gagal', 'Username atau Password salah');
                return redirect()->back();
            }
        }
    }

    public function daftar(Request $request)
    {
        $nis = $request->nis;
        $siswa = siswa::where('nis', '=', $nis)->get();
        if (count($siswa) > 0) {
            $siswa = DB::select('SELECT * FROM siswa WHERE nis = ? AND username IS NOT NULL', [$nis]);
            if(count($siswa) > 0) {
                Alert::error('Gagal', 'Akun sudah terdaftar');
                return redirect()->back();
            } else {
                Alert::success('Berhasil', 'Silahkan buat akun untuk login ke aplikasi');
                return view('daftar', ['nis' => $nis]);
            }
        } else {
            Alert::error('Gagal', 'NIS tidak terdaftar');
            return redirect()->back();
        }
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'file' => 'required|image|max: 4096',
        ]);
        $foto = $request->file('file');
        $uploadlocation = 'dist/img/siswa';
        $nis = $request->nis;
        $username = $request->username;
        $password = $request->password;
        $petugas = petugas::where('username', '=', $username)->get();
        if (count($petugas) > 0) {
            Alert::error('Gagal', 'Username telah dipakai');
            return redirect()->back();
        } else {
            $siswa = siswa::where('username', '=', $username)->get();
            if (count($siswa) > 0) {
                Alert::error('Gagal', 'Username telah dipakai');
                return redirect()->back();
            } else {
                //Buat Akun
                $siswa = siswa::find($nis);
                $siswa->username = $username;
                $siswa->password = sha1($password);
                $siswa->save();
                $foto->move($uploadlocation, $request->nis);
                Alert::success('Berhasil', 'Silahkan login ke aplikasi');
                return redirect('/');
            }
        }
    }


    public function logout(Request $request){
        $request->session()->forget('id_petugas');
        return redirect('/');
    }


}
