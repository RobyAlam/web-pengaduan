<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class kelas extends Model
{
    public $incrementing = false;
    protected $table = "kelas";
    protected $primaryKey = "id_kelas";
    protected $fillable = ['id_kelas', 'nama'];

}
