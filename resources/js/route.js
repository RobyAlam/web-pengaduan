import Vue from 'vue';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

import Master from './View/Master';
import MasterPetugas from './View/MasterPetugas';
import MasterSiswa from './View/MasterSiswa';
import Login from './View/Login';
import FirstPage from './View/FirstPage';
import Pengaduan from './View/Pengaduan';
import DetailPengaduan from './View/DetailPengaduan';

//UNTUK SISWA
import PengaduanSaya from './View/PengaduanSaya';
import SiswaDashboard from './View/Siswa/Dashboard';
//UNTUK PETUGAS
import PetugasDetailPengaduan from './View/Petugas/DetailPengaduan';
import PetugasPengaduan from './View/Petugas/Pengaduan';
import DashboardPetugas from './View/Petugas/DashboardPetugas';
//KHUSUS ADMIN
import Laporan from './View/Petugas/Laporan';
import Siswa from './View/Siswa';
import Kelas from './View/Kelas';
import Petugas from './View/Petugas';
import DashboardAdmin from './View/Petugas/DashboardAdmin';

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            component: Login
        },
        {
            path: '/BuatAkun',
            component: FirstPage
        },
        {
            path: '/Administrator',
            component: Master,
            children: [
                {
                    path: 'Siswa',
                    component: Siswa
                },
                {
                    path: 'Dashboard',
                    component: DashboardAdmin
                },
                {
                    path: 'Kelas',
                    component: Kelas
                },
                {
                    path: 'Petugas',
                    component: Petugas
                },
                {
                    path: 'Pengaduan',
                    component: PetugasPengaduan
                },
                {
                    path: 'Laporan',
                    component: Laporan
                },
                {
                    path: ':id',
                    component: PetugasDetailPengaduan
                },
            ]
        },
        {
            path: '/Petugas',
            component: MasterPetugas,
            children: [
                {
                    path: 'Dashboard',
                    component: DashboardPetugas
                },
                {
                    path: 'Pengaduan',
                    component: PetugasPengaduan
                },
                {
                    path: ':id',
                    component: PetugasDetailPengaduan
                },
            ]
        },
        {
            path: '/Siswa',
            component: MasterSiswa,
            children: [
                {
                    path: 'Dashboard',
                    component: SiswaDashboard
                },
                {
                    path: 'Pengaduan',
                    component: Pengaduan
                },
                {
                    path: 'Pengaduan Saya',
                    component: PengaduanSaya
                },
                {
                    path: ':id',
                    component: DetailPengaduan
                }
            ]
        },
    ]
})

export default router;
