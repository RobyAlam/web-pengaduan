<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900%7CRoboto+Mono:500%7CMaterial+Icons" media="all">
    <title>Pengaduan | SMK TI Pembangunan Cimahi</title><link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
    <script src="{{asset('js/app.js')}}" defer></script>
    <link rel="icon" href="/dist/img/logo.png">
    <link rel="stylesheet" href="/css/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css">
    <script src="/js/sweetalert2/sweetalert2.min.js"></script>
    <script>window._Swal = Swal</script>
    <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
    <body>
        <div id="app"></div>
    </body>
</html>
