<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Daftar | SMK TI Pembangunan Cimahi</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="/plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition register-page" style="background-image: url('/dist/img/background.jpg');">
    @include('sweetalert::alert')
    <div class="register-box">
  <div class="register-logo">
    <a style="color: white" href="/"><b>Pengaduan</b> Siswa</a>
  </div>

  <div class="card">
    <div class="card-body register-card-body">
      <p class="login-box-msg">Mendaftar keanggotaan baru</p>
      <form action="/register" method="post" id="formregister" enctype="multipart/form-data">
          {{ csrf_field() }}
        <div class="input-group mb-3">
          <input type="text" class="form-control" name="nis" value="{{$nis}}" placeholder="NIS" readonly>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="text" class="form-control" name="username" placeholder="Username" maxlength="20">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" name="password" placeholder="Password" maxlength="12">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" name="retype" placeholder="Retype password" maxlength="12">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="form-group">
            <div class="input-group">
                <div class="custom-file">
                    <input type="file" class="custom-file-input" id="exampleInputFile" name="file">
                    <label class="custom-file-label" for="exampleInputFile" id="filename">Pilih foto</label>
                </div>
            </div>
        </div>
        <button type="submit" class="btn btn-primary btn-block">Daftar</button>
      </form>


    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
<!-- /.register-box -->

<!-- jQuery -->
<script src="/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="/dist/js/adminlte.min.js"></script>
<script type="text/javascript">
    $('#formregister').submit(function() {
        var password = $('#formregister').find('[name="password"]').val();
        var retype = $('#formregister').find('[name="retype"]').val();
        var cek = 0;
        if (password == retype) {
            cek = 1;
        }
        if (cek == 0) {
            return false;
        } else {
            return true;
        }
    });


  $('#exampleInputFile').change(function(){
      var filename = this.files[0].name;
      document.getElementById('filename').innerHTML = filename;
    $('#formregister').find('[name="file"]').val(filename);
  })
</script>
</body>
</html>
