<html>
<head>
	<title>Laporan Pengaduan</title>
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 8pt;
		}
	</style>
	<center>
		<h5>SMK TI PEMBANGUNAN CIMAHI</h4>
		<h6>Jl. Haji Bakar, Utama, Kec. Cimahi Selatan, Kota Cimahi, Jawa Barat 40521</h5>
	</center>

    <hr/>
    <h6>Laporan Pengaduan</h6>
    <h6></h6>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>ID Pengaduan</th>
				<th>Judul</th>
				<th>Nama Pengadu</th>
				<th>NIS</th>
				<th>Kelas</th>
				<th>Tanggal</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
            @foreach ($pengaduan as $p)
                <tr>
                    <td>{{$p->id_pengaduan}}</td>
                    <td>{{$p->judul}}</td>
                    <td>{{$p->nama}}</td>
                    <td>{{$p->nis}}</td>
                    <td>{{$p->tingkat}} {{$p->jurusan}} {{$p->kelas}}</td>
                    <td>{{$p->created_at}}</td>
                    <td>{{$p->status}}</td>
                </tr>
            @endforeach
		</tbody>
	</table>

</body>
</html>
