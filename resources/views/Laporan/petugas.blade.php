<html>
<head>
	<title>Laporan Petugas</title>
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 8pt;
		}
	</style>
	<center>
		<h5>SMK TI PEMBANGUNAN CIMAHI</h4>
		<h6>Jl. Haji Bakar, Utama, Kec. Cimahi Selatan, Kota Cimahi, Jawa Barat 40521</h5>
	</center>

    <hr/>
    <h6>Laporan Petugas</h6>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>No</th>
				<th>ID</th>
				<th>Nama</th>
				<th>Hak Akses</th>
				<th>Jenis Kelamin</th>
				<th>Telepon</th>
				<th>Alamat</th>
			</tr>
		</thead>
		<tbody>
            @php
                $count = 1;
            @endphp
            @foreach ($petugas as $p)
                <tr>
                    <td>{{$count}}</td>
                    <td>{{$p->id_petugas}}</td>
                    <td>{{$p->nama}}</td>
                    <td>{{$p->level}}</td>
                    <td>{{$p->jk}}</td>
                    <td>{{$p->telp}}</td>
                    <td>{{$p->alamat}}</td>
                </tr>
                @php
                    $count++;
                @endphp
            @endforeach
		</tbody>
	</table>

</body>
</html>
