<html>
<head>
	<title>Daftar Siswa</title>
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
</head>
<body>
	<style type="text/css">
		table tr td,
		table tr th{
			font-size: 8pt;
		}
	</style>
	<center>
		<h5>SMK TI PEMBANGUNAN CIMAHI</h4>
		<h6>Jl. Haji Bakar, Utama, Kec. Cimahi Selatan, Kota Cimahi, Jawa Barat 40521</h5>
	</center>

    <hr/>
    <h6>Daftar Siswa</h6>
    <h6></h6>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>NIS</th>
				<th>Nama</th>
				<th>Kelas</th>
				<th>Jenis Kelamin</th>
				<th>Telepon</th>
				<th>Tanggal Lahir</th>
			</tr>
		</thead>
		<tbody>
            @foreach ($siswa as $s)
                <tr>
                    <td>{{$s->nis}}</td>
                    <td>{{$s->nama}}</td>
                    <td>{{$s->tingkat}} {{$s->jurusan}} {{$s->kelas}}</td>
                    <td>{{$s->jk}}</td>
                    <td>{{$s->telp}}</td>
                    <td>{{$s->tgl_lahir}}</td>
                </tr>
            @endforeach
		</tbody>
	</table>

</body>
</html>
