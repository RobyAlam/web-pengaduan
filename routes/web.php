<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Controller@cekakun');
Route::get('/BuatAkun', 'Controller@formbuatakun');
Route::post('/Create/Admin', 'Controller@BuatAdmin');

Route::post('/login', 'CRUDController@login');
Route::post('/register', 'Controller@register');
Route::get('/daftar', 'Controller@daftar');
Route::get('/logout', 'Controller@logout');

Route::get('/Petugas/{any}', 'ViewController@Petugas')->where('any', '.*');

Route::get('/Administrator/{any}', 'ViewController@Admin')->where('any', '.*');

Route::get('/Siswa/{any}', 'ViewController@Siswa')->where('any', '.*');


//Route untuk mengambil data dari DB
Route::get('/GetUserInfo', 'CRUDController@userinfo');
Route::get('/GetSiswaKelas', 'CRUDController@siswa_kelas');
Route::get('/GetKelas', 'CRUDController@kelas');
Route::get('/GetTingkat', 'CRUDController@tingkat');
Route::get('/GetJurusan', 'CRUDController@jurusan');
Route::get('/GetPetugas', 'CRUDController@petugas');
Route::get('/GetPengaduan', 'CRUDController@pengaduan');
Route::get('/GetPengaduan/Search/{key}', 'CRUDController@CariPengaduan');
Route::get('/GetPengaduanLimit', 'CRUDController@pengaduanLimit');
Route::get('/GetPengaduanSaya', 'CRUDController@pengaduansaya');
Route::get('/GetTanggapan', 'CRUDController@tanggapansemua');
Route::get('/GetKomentar/{id_pengaduan}', 'CRUDController@komentar');
Route::get('/GetTanggapan/{id_pengaduan}', 'CRUDController@tanggapan');
Route::get('/GetPesan/{id_pengaduan}', 'CRUDController@pesan');
Route::get('/KodeOtomatis', 'CRUDController@KodeOtomatis');
Route::get('/GetPengaduan/{id_pengaduan}', 'CRUDController@pengaduanID');
Route::get('/GetNIS/{nis}', 'CRUDController@CariNIS');

//Route untuk melakukan operasi Delete data dari DB
Route::post('/Delete/Siswa', 'CRUDController@DeleteSiswa');
Route::post('/Delete/Tingkat', 'CRUDController@DeleteTingkat');
Route::post('/Delete/Kelas', 'CRUDController@DeleteKelas');
Route::post('/Delete/Jurusan', 'CRUDController@DeleteJurusan');
Route::post('/Delete/Petugas', 'CRUDController@DeletePetugas');

//Route untuk melakukan operasi Insert atau Update ke DB
Route::post('/Insert/Siswa', 'CRUDController@InsertSiswa');
Route::post('/Insert/Tingkat', 'CRUDController@InsertTingkat');
Route::post('/Insert/Kelas', 'CRUDController@InsertKelas');
Route::post('/Insert/Jurusan', 'CRUDController@InsertJurusan');
Route::post('/Insert/Petugas', 'CRUDController@InsertPetugas');
Route::post('/Insert/Komentar', 'CRUDController@InsertKomentar');
Route::post('/Insert/Tanggapan', 'CRUDController@InsertTanggapan');
Route::post('/Insert/Pesan', 'CRUDController@InsertPesan');
Route::post('/Insert/Pengaduan', 'CRUDController@InsertPengaduan');
Route::post('/Update/Status', 'CRUDController@UpdateStatus');
Route::post('/Update/Siswa', 'CRUDController@BuatAkun');
Route::post('/Update/Account/Siswa', 'CRUDController@UpdateAkunSiswa');
Route::post('/Update/Account/Petugas', 'CRUDController@UpdateAkunPetugas');

Route::get('/Laporan/Petugas', 'CRUDController@LaporanPetugas');
Route::get('/Laporan/Semua/Siswa', 'CRUDController@LaporanSemuaSiswa');
Route::get('/Laporan/Semua/Pengaduan', 'CRUDController@LaporanSemuaPengaduan');
Route::post('/Laporan/Siswa', 'CRUDController@LaporanSiswa');
Route::post('/Laporan/Pengaduan', 'CRUDController@LaporanPengaduan');
